package com.bidjoka.cm.airquality.activite;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.ButterKnife;

import com.bidjoka.cm.airquality.R;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class MapLocation extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_location);

        // Binding ButterKnife annotations now that content view has been set
        ButterKnife.bind(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.accueil:
                startActivity(new Intent(this, MainActivity.class));
                break;
            case R.id.liste:
                startActivity(new Intent(this, ListeZone.class));
                break;
            case R.id.map:
                startActivity(new Intent(this, MapZone.class));
                break;
            case R.id.favoris:
                startActivity(new Intent(this, Favoris.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
