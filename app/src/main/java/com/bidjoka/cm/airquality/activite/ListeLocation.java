package com.bidjoka.cm.airquality.activite;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.os.Bundle;
import android.widget.EditText;

import com.bidjoka.cm.airquality.R;
import com.bidjoka.cm.airquality.ui.LocationAdapter;

import java.util.ArrayList;

public class ListeLocation extends AppCompatActivity {

    @BindView(R.id.location_recyclerView)
    RecyclerView mRecyclerView;
    private LocationAdapter mLocationAdapter;

    @BindView(R.id.location_edittext)
    EditText mSearchEditText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liste_location);

        // Binding ButterKnife annotations now that content view has been set
        ButterKnife.bind(this);

        // Instanciation d'un LocationAdpater vide
        mLocationAdapter = new LocationAdapter(this, new ArrayList<>());
        mRecyclerView.setAdapter(mLocationAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @OnClick(R.id.location_accueil)
    public void acceuil(){

    }

    @OnClick(R.id.location_favoris)
    public void favoris(){

    }

    @OnClick(R.id.location_liste)
    public void liste(){

    }

    @OnClick(R.id.location_carte)
    public void carte(){

    }
}
