package com.bidjoka.cm.airquality.event;

import com.bidjoka.cm.airquality.model.Zone;

import java.util.List;

public class ZoneResultEvent {

    private List<Zone> zones;

    public ZoneResultEvent(List<Zone> zones){
        this.zones = zones;
    }
     public List<Zone> getZones(){
        return zones;
     }
}
