package com.bidjoka.cm.airquality.event;

import com.bidjoka.cm.airquality.model.Temperature;

import java.util.List;

public class TemperatureResultEvent {

    private List<Temperature> temperatures;

    public TemperatureResultEvent(List<Temperature> temperatures) {
        this.temperatures = temperatures;
    }

    public List<Temperature> getTemperatures() {
        return temperatures;
    }
}
