package com.bidjoka.cm.airquality.activite;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.bidjoka.cm.airquality.R;
import com.bidjoka.cm.airquality.model.Zone;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import java.util.LinkedHashMap;
import java.util.Map;

public class MapZone extends AppCompatActivity implements OnMapReadyCallback {

    @BindView(R.id.map_zone_edittext)
    EditText mSearchEditText;

    private Map<String, Zone> mMarkersToPlaces = new LinkedHashMap<>();

    private GoogleMap mActiveGoogleMapZone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_zone);

        // Binding ButterKnife annotations now that content view has been set
        ButterKnife.bind(this);

        // Get map fragment
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapZone);
        mapFragment.getMapAsync(this);


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mActiveGoogleMapZone = googleMap;
        mActiveGoogleMapZone.getUiSettings().setZoomControlsEnabled(true);
        mActiveGoogleMapZone.setMapType(GoogleMap.MAP_TYPE_SATELLITE);


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.accueil:
                startActivity(new Intent(this, MainActivity.class));
                break;
            case R.id.liste:
                startActivity(new Intent(this, ListeZone.class));
                break;
            case R.id.map:
                startActivity(new Intent(this, MapZone.class));
                break;
            case R.id.favoris:
                startActivity(new Intent(this, Favoris.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
