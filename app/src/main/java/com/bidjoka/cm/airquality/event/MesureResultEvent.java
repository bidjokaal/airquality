package com.bidjoka.cm.airquality.event;

import com.bidjoka.cm.airquality.model.Mesure;

import java.util.List;

public class MesureResultEvent {

    private List<Mesure> mesures;

    public MesureResultEvent(List<Mesure> mesures) {
        this.mesures = mesures;
    }

    public List<Mesure> getMesures() {
        return mesures;
    }
}
