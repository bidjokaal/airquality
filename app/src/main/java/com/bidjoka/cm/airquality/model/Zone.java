package com.bidjoka.cm.airquality.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;

import java.util.List;

@Table(name = "zone")
public class Zone extends Model {


    @Expose
    @Column(name = "city", index = true, unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    public String city;

    @Column(name = "name")
    public String locationName;

    public List<Location> locations;

    public Coordonnee coordinates;

    public List<Location> getLocation(){
        return locations;
    }

    //récupération de la latitude, la longitude, et du nom de la zone dans la base de données
    public Coordonnee getCoordinates(){
        return coordinates;
    }

    public String getCity(){
        return city;
    }


}
