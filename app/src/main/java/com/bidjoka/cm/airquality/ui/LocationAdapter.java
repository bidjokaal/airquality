package com.bidjoka.cm.airquality.ui;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bidjoka.cm.airquality.R;
import com.bidjoka.cm.airquality.model.Location;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.LocationViewHolder>{

    private LayoutInflater inflater;
    private Activity context;
    private List<Location> mlocations;

    public LocationAdapter(Activity context, List<Location> locations) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.mlocations = locations;
    }

    @NonNull
    @Override
    public LocationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.location_item, parent, false);
        LocationAdapter.LocationViewHolder holder = new LocationAdapter.LocationViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull LocationViewHolder holder, int position) {
        final Location location = mlocations.get(position);
    }

    @Override
    public int getItemCount() {
        return mlocations.size();
    }

    class LocationViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.location_name)
        TextView mLocationNameTextView;

        @BindView(R.id.location_temperature)
        TextView mTemperatureTextView;

        @BindView(R.id.location_indicateur)
        TextView mIndicateurTextView;
        public LocationViewHolder(View itemView){
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
