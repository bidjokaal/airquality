package com.bidjoka.cm.airquality.activite;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.bidjoka.cm.airquality.R;
import com.bidjoka.cm.airquality.event.EventBusManager;
import com.bidjoka.cm.airquality.service.ZoneSearchService;
import com.bidjoka.cm.airquality.ui.ZoneAdapter;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import java.util.ArrayList;

public class ListeZone extends AppCompatActivity {

    @BindView(R.id.zone_recyclerView)
    RecyclerView mRecyclerView;
    private ZoneAdapter mZoneAdapter;

    @BindView(R.id.zone_edittext)
    EditText mSearchEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liste_zone);

        // Binding ButterKnife annotations now that content view has been set
        ButterKnife.bind(this);

        // Instanciation d'un LocationAdpater vide
        mZoneAdapter = new ZoneAdapter(this, new ArrayList<>());
        mRecyclerView.setAdapter(mZoneAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
    /*
    @Override
    protected void onResume() {
        // Do NOT forget to call super.onResume()
        super.onResume();

        // Register to Event bus : now each time an event is posted, the activity will receive it if it is @Subscribed to this event
        EventBusManager.BUS.register(this);

        // Refresh search
        ZoneSearchService.INSTANCE.searchZoneFromCountry()(mSearchEditText.getText().toString());
    }
    */
    @Override
    protected void onPause() {
        // Unregister from Event bus : if event are posted now, the activity will not receive it
        EventBusManager.BUS.unregister(this);

        super.onPause();
    }

   @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.accueil:
                startActivity(new Intent(this, MainActivity.class));
                break;
            case R.id.liste:
                startActivity(new Intent(this, ListeZone.class));
                break;
            case R.id.map:
                startActivity(new Intent(this, MapZone.class));
                break;
            case R.id.favoris:
                startActivity(new Intent(this, Favoris.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
