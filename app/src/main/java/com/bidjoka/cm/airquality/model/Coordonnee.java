package com.bidjoka.cm.airquality.model;


import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

@Table(name = "coordonnee")
public class Coordonnee extends Model {

    @Column(name = "id", index = true, unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    public String id;

    @Expose
    public List<Double> coordinates = new ArrayList<>();

    @Column(name = "latitude")
    public double latitude = 0;

    @Column(name = "longitude")
    public double longitude = 0;

    public String ConcatCoordonnee(){
        String concat = ""+latitude+"."+longitude;
        return concat;
    }
}
