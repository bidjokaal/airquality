package com.bidjoka.cm.airquality.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

public class Mesure extends Model {

    @Column(name = "id", index = true, unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    public String id;

    @Column(name = "mesures")
    public String mesures;

    @Expose
    public List<Mesure> measurements = new ArrayList<>();

    @Expose
    public String parameter;

    @Expose
    public double value;

    //récupération de toutes les mesures et concaténation dans une même string
    public String getAllMesure(List<Mesure> mesures){
        int i = 0;
        int b = mesures.size();
        String tampon = "";
        while(b!=0){
            String ind = mesures.get(i).parameter;
            double val = mesures.get(i).value;
            tampon = tampon + "indicateur "+ind+" valeur "+val+"\n";
            i++;
            b = b - i;
        }
        return tampon;
    }

}
