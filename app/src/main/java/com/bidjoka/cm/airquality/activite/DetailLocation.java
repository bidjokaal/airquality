package com.bidjoka.cm.airquality.activite;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.ButterKnife;

import com.bidjoka.cm.airquality.R;

import android.os.Bundle;

public class DetailLocation extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_location);

        // Binding ButterKnife annotations now that content view has been set
        ButterKnife.bind(this);
    }
}
