package com.bidjoka.cm.airquality.ui;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bidjoka.cm.airquality.R;
import com.bidjoka.cm.airquality.model.Zone;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ZoneAdapter extends RecyclerView.Adapter<ZoneAdapter.ZoneViewHolder>{

    private LayoutInflater inflater;
    private Activity context;
    private List<Zone> mZones;

    public ZoneAdapter(Activity context, List<Zone> zones) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.mZones = zones;
    }

    @Override
    public ZoneViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.zone_item, parent, false);
        ZoneAdapter.ZoneViewHolder holder = new ZoneAdapter.ZoneViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ZoneViewHolder holder, int position) {
        final Zone zone = mZones.get(position);
        /*
        holder.mZoneNameTextView.setText(zone.getProperties().name);
        holder.mZoneIcon.setImageResource(R.drawable.zone);
        holder.ZoneIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Open place details activity
                Intent seeLocationIntent = new Intent(context, ListeLocation.class);
                seeLocaionIntent.putExtra("zoneName", zone.getProperties().name);
                context.startActivity(seeLocationIntent);
            }
        });*/

    }

    @Override
    public int getItemCount() {
        return mZones.size();
    }

    public void setmZones(List<Zone> zones){
        this.mZones = zones;
    }

    class ZoneViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.zone_name)
        TextView mZoneNameTextView;

        @BindView(R.id.zone_adapter_icon)
        ImageView mZoneIcon;

        public ZoneViewHolder(View itemView){
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
