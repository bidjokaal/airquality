package com.bidjoka.cm.airquality.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

@Table(name = "Location")
public class Location extends Model {

    @Expose
    public String location;

    @Column(name = "id", index = true, unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    public String id;

    @Expose
    @Column(name = "city", index = true, unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    public String city;

    /* il manque la forme correcte de l'API
    @Expose
    private Temperature temperature;
     */

    @Expose
    public Mesure measurements;

    @Expose
    private Coordonnee coordinates;


    //recherche des mesures contenues dans la base de données
    public Mesure getMesure(){
        return measurements;
    }

    //recherche des coordonnées de la location contenues dans la base de données
    public Coordonnee getCoordinates(){
        return coordinates;
    }

    /*
    public Temperature getTemperature(){
        return temperature;
    }*/

}
